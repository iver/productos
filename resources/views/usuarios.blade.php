
@extends('layouts.menu')

@section('dashboard')
 



                <!-- Begin Page Content -->
                <div class="container-fluid">

                   <div id="listausuarios">
                     <!-- Page Heading -->
                     <h1 class="h3 mb-2 text-gray-800">Listado de usuarios</h1>  
                     <div class="card shadow mb-4">
                         <div class="card-body">
                            <div class="float-right"> <button type="button" class="btn btn-primary" id="addUsuarios">Registrar</button> <br></div>
                             <div class="table-responsive">
                                 <table class="table table-bordered" id="lstUsuarios" width="100%" cellspacing="0">
                                     <thead>
                                         <tr>
                                             <th>Nombre</th>
                                             <th>Paterno</th>
                                             <th>Materno</th>
                                             <th>Usuario</th>
                                             <th colspan="2" style="width: 2%;">Acciones</th>
                                         </tr>
                                     </thead>
                                     <tbody>
                                     </tbody>
                                 </table>
                             </div>
                         </div>
                     </div>
                   </div>
                   <div id="formUsuarios">
                      <div class="alert alert-danger" role="alert" id="diverror">
                        
                      </div>
                       <form method="post" id="usuarioForm" enctype="multipart/form-data">
                            <input type="hidden" class="form-control" id="id" name="id">
                            <div class="form-group">
                                <label for="name">Nombre(s)</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre(s)">
                            </div>
                            <div class="form-group">
                                <label for="name">Apellido Paterno</label>
                                <input type="text" class="form-control" id="paterno" name="paterno" placeholder="Apellido Paterno">
                            </div>
                            <div class="form-group">
                                <label for="name">Apellido Materno</label>
                                <input type="text" class="form-control" id="materno" name="materno" placeholder="Apellido Materno">
                            </div>
                            <div class="form-group">
                                <label for="name">Correo electronico</label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Correo electronico">
                            </div>
                            <div class="form-group">
                                <label for="name">Contraseña</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña">
                            </div>
                            <div class="form-group">
                                <label for="name">Confirmar Contraseña</label>
                                <input type="password" class="form-control" id="confirmationp" name="confirmationp" placeholder="Confirmar Contraseña">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Rol</label>
                                <select class="form-control" id="cmbrol" name="id_rol">
                                </select>
                            </div>

                            <br> <br>

                            <div class="row">
                                <div class="col-md-9">
                                </div>
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-info" id="cancelUsuario" onclick="editUsuario()">Cancelar</button>&nbsp;&nbsp;&nbsp;
                                    <button type="submit" class="btn btn-primary" id="saveUsuario">Guardar</button>
                                </div>
                                
                            </div>

                       </form>
                   </div>





                   



                </div>


   
                <script>
                       //https://sweetalert2.github.io/#download

                    $(document).ready(function(){

                        $("#listausuarios").show();
                        $("#formUsuarios").hide();
                        $("#diverror").hide();
                
                       $( "#addUsuarios" ).click(function() {
                            $("#id").val('');
                            $("#nombre").val('');
                            $("#paterno").val('');
                            $("#materno").val(''); 
                            $("#email").val('');                            
                            $("#id_rol").val('');                            
                            $("#code_role").val('');                  
                            $("#password").val('');
                            $("#confirmationp").val('');
                            $("#listausuarios").hide();
                            $("#formUsuarios").show();

                            $('#cmbrol').empty();
                            $.ajax({
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'{{url('allroles')}}',
                                method: 'POST',
                                data:{ }
                            }).done((res) =>{
                                $('#cmbrol').empty();
                                let resp = JSON.parse(res);
                                if(resp && resp.roles){
                                    resp.roles.forEach( function(valor, indice, array) {
                                        $('#cmbrol').append(                                           
                                            $('<option></option>').val(valor.id).html(valor.nombre)
                                        );
                                    });
                                }
                            })


                            


                       });
                
                       $( "#cancelUsuario" ).click(function() {
                            $("#listausuarios").show();
                            $("#formUsuarios").hide();
                       });
                
                       listausuarios();
                
                       function listausuarios(){
                            $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'/user/all',
                                method: 'POST',
                                data:{}
                                }).done((res) =>{
                                    let resp = JSON.parse(res);
                                    if(resp){
                                        $("#lstUsuarios").find('tbody').empty();                
                                        var data_table = '';
                                        for(var i= 0; i< resp.length; i++){
                                            data_table += "<tr>";
                                            data_table += "<td>"+resp[i].nombre+"</td>";
                                            data_table += "<td>"+resp[i].paterno+"</td>";
                                            data_table += "<td>"+resp[i].materno+"</td>";
                                            data_table += "<td>"+resp[i].email+"</td>";
                                            data_table += '<td><button class="btn btn-primary" style="font-size:12px;" onClick=editUser('+resp[i].id+')><i class="fa fa-edit"></i></button>';
                                            data_table += '<td><button class="btn btn-danger" style="font-size:12px;" onClick=deleteUsuario('+resp[i].id+') ><i class="fa fa-trash"></i></button>';
                                            data_table += "</tr>";
                                        }
                                        $("#lstUsuarios").find('tbody').append(data_table);                
                                }
                                
                                $("#listausuarios").show();
                                $("#formUsuarios").hide();
                            })
                       }
                
                
                       $( "#usuarioForm" ).on('submit', function(event) {
                            event.preventDefault();
                            $("#diverror").empty();
                            $("#diverror").hide();
                            

                            var error = false;
                            if(!$("#nombre").val()){
                                error = true;
                                $("#diverror").append('<label>Ingrese un nombre</label><br>');
                            }
                            if(!$("#paterno").val()){
                                error = true;
                                $("#diverror").append('<label>Ingrese apellido paterno</label><br>');
                            }

                            if(!$("#email").val()){
                                error = true;
                                $("#diverror").append('<label>Ingrese un email</label><br>');
                            }
                            if(error){
                                $("#diverror").show();
                                return;
                            }

                            if($("#id").val() == ''){
                               if($("#password").val() != $("#confirmationp").val() || $("#password").val() == ''){
                                    $("#diverror").append('La confirmación de la contraseña es incorrecta');
                                    $("#diverror").show();
                                     return ;
                               }else{
                               }
                            }
                            


                            
                            $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url: "{{url('saveUser')}}",
                                method:"POST",
                                data:new FormData(this),
                                dataType:'JSON',
                                contentType: false,
                                cache: false,
                                processData: false,
                                success:function(data){
                                    if(data && data.error){
                                        $("#diverror").show();
                                        for(var i = 0; i<data.error.length; i++){
                                            $("#diverror").append('<label>'+data.error[i]+'</label><br>');
                                        }
                                    }else{
                                        listausuarios();
                                        $("#listausuarios").show();
                                        $("#formUsuarios").hide();
                                    }
                                    
                                },
                                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                                    console.log("Status: " + textStatus);
                                } 
                            })
                       });
                       
                
                
                
                      
                    })//fin de $(document)
                
                    function deleteUsuario(id){                       
                            Swal.fire({
                                title: 'Eliminación',
                                text: "¿Esta seguro de eliminar el registro?",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Eliminar'
                            }).then( 
                                function (res) {
                                    if(res.isConfirmed)
                                        deleteUsuarioItem(id);
                                },
                            );
                       }
                       

                       function deleteUsuarioItem(id){
                            $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'{{url('deleteUser')}}',
                                method: 'POST',
                                data:{ id:id }
                            }).done((res) =>{
                                Swal.fire( 'Eliminación!', 'Registro eliminado correctamente.', 'success' )
                                listausuarios();
                                $("#listausuarios").show();
                                $("#formUsuarios").hide();  
                            
                            })
                       }
                
                       function editUser(id){
                            $("#listausuarios").hide();
                            $("#formUsuarios").show();

                            
                            $("#password").val('');
                            $("#confirmationp").val('');

                            $.ajax({
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'{{url('editUser')}}',
                                method: 'POST',
                                data:{ id:id }
                            }).done((res) =>{
                                let resp = JSON.parse(res);
                                console.log("resp >> ", resp)
                                if(resp && resp.user){
                                    $("#id").val(resp.user.id);
                                    $("#nombre").val(resp.user.nombre);
                                    $("#paterno").val(resp.user.paterno);
                                    $("#materno").val(resp.user.materno); 
                                    $("#email").val(resp.user.email);                            
                                    $("#id_rol").val(resp.user.id_rol);                            
                                    $("#code_role").val(resp.user.code_role);    
                                    $("#listausuarios").hide();
                                    $("#formUsuarios").show();

                                    $('#cmbrol').empty();

                                    resp.roles.forEach( function(valor, indice, array) {
                                        var select = "";
                                        if(valor.id == resp.user.id_rol){
                                            select = 'selected="selected"';
                                        }
                                        $('#cmbrol').append(                                           
                                            $('<option '+select+'></option>').val(valor.id).html(valor.nombre)
                                        );
                                    });
                                }
                            })
                       }


                       
                       function listausuarios(){
                            $.ajax({
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                url:'/user/all',
                                method: 'POST',
                                data:{}
                            }).done((res) =>{
                                let resp = JSON.parse(res);
                                if(resp){
                                    $("#lstUsuarios").find('tbody').empty();
                                    var data_table = '';
                                    for(var i= 0; i< resp.length; i++){
                                        data_table += "<tr>";
                                        data_table += "<td>"+resp[i].nombre+"</td>";
                                        data_table += "<td>"+resp[i].paterno+"</td>";
                                        data_table += "<td>"+resp[i].materno+"</td>";
                                        data_table += "<td>"+resp[i].email+"</td>";
                                        data_table += '<td><button class="btn btn-primary" style="font-size:12px;" onClick=editUser('+resp[i].id+')><i class="fa fa-edit"></i></button>';
                                        data_table += '<td><button class="btn btn-danger" style="font-size:12px;" onClick=deleteUsuario('+resp[i].id+') ><i class="fa fa-trash"></i></button>';
                                        data_table += "</tr>";
                                    }
                                    $("#lstUsuarios").find('tbody').append(data_table);
                                }
                                $("#listausuarios").show();
                                $("#formUsuarios").hide();
                            })
                       }
                
                
                </script>
                


@endsection
