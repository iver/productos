
@extends('layouts.menu')

@section('dashboard')
 



                <!-- Begin Page Content -->
                <div class="container-fluid">

                   <div id="listaproductos">
                     <!-- Page Heading -->
                     <h1 class="h3 mb-2 text-gray-800">Listado de productos</h1>  
                     <div class="card shadow mb-4">
                         <div class="card-body">
                            <div class="float-right"> <button type="button" class="btn btn-primary" id="addProduct">Registrar</button> <br></div>
                             <div class="table-responsive">
                                 <table class="table table-bordered" id="lstProducts" width="100%" cellspacing="0">
                                     <thead>
                                         <tr>
                                             <th>Nombre</th>
                                             <th>Descripción</th>
                                             <th>Costo</th>
                                             <th>Stock</th>
                                             <th>Status</th>
                                             <th>Comentario</th>
                                             <th colspan="2" style="width: 2%;">Acciones</th>
                                         </tr>
                                     </thead>
                                     <tbody>
                                       
                                        
                                     </tbody>
                                 </table>
                             </div>
                         </div>
                     </div>
                   </div>
                   <div id="formProductos">


                        <div class="alert alert-danger" role="alert" id="error">                        
                        </div>
                       <form method="post" id="productForm" enctype="multipart/form-data">
                            <input type="hidden" class="form-control" id="id" name="id">
                            <div class="form-group">
                                <label for="name">Nombre</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">
                            </div>
                            <div class="form-group">
                                <label for="name">Descripción</label>
                                <textarea class="form-control" id="descripcion" name="descripcion" rows="3"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="name">Costo</label>
                                <input type="number" class="form-control" id="costo" name="costo" placeholder="Costo">
                            </div>
                            <div class="form-group">
                                <label for="name">Stock</label>
                                <input type="number" class="form-control" id="stock" name="stock" placeholder="Stock">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Categoria</label>
                                <select class="form-control" id="cmbcategory" name="id_categoria">
                                </select>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image" name="image" lang="es">
                                <label class="custom-file-label" for="image">Seleccionar Archivo</label>
                            </div>
                            <br>
                            <div class="row" id="divframeProduct">
                                <div class="col-md-2"></div> 
                                <div class="col-md-6"><img id="frameProduct" width="500" height="400" /></div>
                                <div class="col-md-4"></div>                                
                            </div>
                            

                            
                            <div class="row" style="width: 100%;padding:20px;" id="divcomentario">
                                <div class="form-group" style="width: 100%;padding:20px;">
                                    <label for="name">Comentario</label>
                                    <textarea class="form-control" id="comentario" name="comentario" rows="3"></textarea>
                                </div>                                
                            </div>
                            <br> <br> 
                            
                            <div class="row">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-info" id="cancelProduct" onclick="editProduct()">Cancelar</button>&nbsp;&nbsp;&nbsp;
                                    <button type="submit" class="btn btn-primary" id="saveProduct" onClick="SaveProducto(1)">Guardar</button>                                    
                                    <button type="submit" class="btn btn-success" id="aceptarProduct" onClick="SaveProducto(2)">Aceptar</button>
                                    <button type="submit" class="btn btn-danger" id="rechazarProduct" onClick="SaveProducto(3)">Rechazar</button>
                                </div>
                                
                            </div>

                       </form>
                   </div>





                   



                </div>


   
                <script>
                       //https://sweetalert2.github.io/#download

                    $(document).ready(function(){

                        $("#listaproductos").show();
                        $("#formProductos").hide();

                        $("#aceptarProduct").hide();
                        $("#rechazarProduct").hide();
                        $("#divcomentario").hide();
                          
                        $("#error").hide();
                
                       $( "#addProduct" ).click(function() {
                            $("#error").hide();
                            $("#id").val('');
                            $("#nombre").val('');
                            $("#descripcion").val('');
                            $("#costo").val(''); 
                            $("#stock").val('');
                            $("#divframeProduct").hide();
                            $("#listaproductos").hide();
                            $("#formProductos").show();
                            $('#cmbcategory').empty();
                            $("#comentario").val('');

                            $.ajax({
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'{{url('allcat')}}',
                                method: 'POST',
                                data:{ }
                            }).done((res) =>{
                                $('#cmbcategory').empty();
                                let resp = JSON.parse(res);
                                if(resp && resp.category){
                                    resp.category.forEach( function(valor, indice, array) {
                                        $('#cmbcategory').append(                                           
                                            $('<option></option>').val(valor.id).html(valor.nombre)
                                        );
                                    });
                                }
                            })

                       });
                
                       $( "#cancelProduct" ).click(function() {
                            $("#listaproductos").show();
                            $("#formProductos").hide();
                       });
                
                       listaproductos();
                
                       function listaproductos(){
                            $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'/product/all',
                                method: 'POST',
                                data:{}
                                }).done((res) =>{
                                    let response = JSON.parse(res);
                                    let resp = response.results;
                                    if(resp){
                                        $("#lstProducts").find('tbody').empty();                
                                        var data_table = '';
                                        for(var i= 0; i< resp.length; i++){
                                            data_table += "<tr>";
                                            data_table += "<td>"+resp[i].nombre+"</td>";
                                            data_table += "<td>"+resp[i].descripcion+"</td>";
                                            data_table += "<td>"+resp[i].costo+"</td>";
                                            data_table += "<td>"+resp[i].stock+"</td>";

                                            data_table += "<td>";
                                            if(resp[i].status && resp[i].status == 'PENDIENTE'){
                                                data_table += "<button class='btn btn-info' style='font-size:10px;'>POR REVISAR</button>";
                                            }
                                            else if(resp[i].status && resp[i].status == 'ACEPTADO'){
                                                data_table += "<button class='btn btn-success' style='font-size:10px;'>ACEPTADO</button>";
                                            }else if(resp[i].status && resp[i].status == 'RECHAZADO'){
                                                data_table += "<button class='btn btn-danger' style='font-size:10px;'>RECHAZADO</button>";
                                            }
                                            data_table += "</td>";

                                            data_table += "<td>"+resp[i].comentario+"</td>";
                                            data_table += '<td><button class="btn btn-primary" style="font-size:12px;" onClick=editProduct('+resp[i].id+')><i class="fa fa-edit"></i></button>';
                                            data_table += '<td><button class="btn btn-danger" style="font-size:12px;" onClick=deleteProduct('+resp[i].id+') ><i class="fa fa-trash"></i></button>';
                                            data_table += "</tr>";
                                        }
                                        $("#lstProducts").find('tbody').append(data_table);                
                                    }
                                    if(response.validar_producto){                                        
                                        $("#aceptarProduct").show();
                                        $("#rechazarProduct").show();
                                        $("#divcomentario").show();
                                    }
                                
                                    $("#listaproductos").show();
                                    $("#formProductos").hide();
                            })
                       }
                
                      
                    })//fin de $(document)
                
                    function deleteProduct(id){                       
                            Swal.fire({
                                title: 'Eliminación',
                                text: "¿Esta seguro de eliminar el registro?",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Eliminar'
                            }).then( 
                                function (res) {
                                    if(res.isConfirmed)
                                        deleteProductItem(id);
                                }
                            );

                       }
                       

                       function deleteProductItem(id){
                            $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'{{url('deleteProduct')}}',
                                method: 'POST',
                                data:{ id:id }
                            }).done((res) =>{
                                Swal.fire( 'Eliminación!', 'Registro eliminado correctamente.', 'success' )
                                listaproductos();
                                $("#listaproductos").show();
                                $("#formProductos").hide();  
                            
                            })
                       }
                
                       function editProduct(id){
                            $("#listaproductos").hide();
                            $("#formProductos").show();

                            $.ajax({
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'{{url('allcat')}}',
                                method: 'POST',
                                data:{ }
                            }).done((res) =>{
                                $('#cmbcategory').empty();
                                let resp = JSON.parse(res);
                                if(resp && resp.category){
                                    resp.category.forEach( function(valor, indice, array) {
                                        $('#cmbcategory').append(                                           
                                            $('<option></option>').val(valor.id).html(valor.nombre)
                                        );
                                    });
                                }
                            })

                            $.ajax({
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'{{url('editProduct')}}',
                                method: 'POST',
                                data:{ id:id }
                            }).done((res) =>{
                                let resp = JSON.parse(res);
                                if(resp && resp.producto){
                                    $("#id").val(resp.producto.id);
                                    $("#nombre").val(resp.producto.nombre);
                                    $("#descripcion").val(resp.producto.descripcion);
                                    $("#costo").val(resp.producto.costo); 
                                    $("#stock").val(resp.producto.stock);                                    
                                    $("#comentario").val(resp.producto.comentario);
                                }

                                if(resp && resp.file){
                                    $("#divframeProduct").show();
                                    var iframe = document.getElementById("frameProduct");
                                    iframe.setAttribute("src", "/images/"+resp.file.archivo);
                                }
                            })
                       }


                       
                       function listaproductos(){
                            $.ajax({
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                url:'/product/all',
                                method: 'POST',
                                data:{}
                            }).done((res) =>{
                                let response = JSON.parse(res);
                                let resp = response.results;
                                if(resp){
                                    $("#lstProducts").find('tbody').empty();
                                    var data_table = '';
                                    for(var i= 0; i< resp.length; i++){
                                        data_table += "<tr>";
                                        data_table += "<td>"+resp[i].nombre+"</td>";
                                        data_table += "<td>"+resp[i].descripcion+"</td>";
                                        data_table += "<td>"+resp[i].costo+"</td>";
                                        data_table += "<td>"+resp[i].stock+"</td>";
                                        data_table += "<td>";
                                        if(resp[i].status && resp[i].status == 'PENDIENTE'){
                                            data_table += "<button class='btn btn-info' style='font-size:10px;'>POR REVISAR</button>";
                                        }
                                        else if(resp[i].status && resp[i].status == 'ACEPTADO'){
                                            data_table += "<button class='btn btn-success' style='font-size:10px;'>ACEPTADO</button>";
                                        }else if(resp[i].status && resp[i].status == 'RECHAZADO'){
                                            data_table += "<button class='btn btn-danger' style='font-size:10px;'>RECHAZADO</button>";
                                        }
                                        data_table += "</td>";

                                        data_table += "<td>"+resp[i].comentario+"</td>";
                                        data_table += '<td><button class="btn btn-primary" style="font-size:12px;" onClick=editProduct('+resp[i].id+')><i class="fa fa-edit"></i></button>';
                                        data_table += '<td><button class="btn btn-danger" style="font-size:12px;" onClick=deleteProduct('+resp[i].id+') ><i class="fa fa-trash"></i></button>';
                                        data_table += "</tr>";
                                    }
                                    $("#lstProducts").find('tbody').append(data_table);
                                }
                                $("#listaproductos").show();
                                $("#formProductos").hide();
                            })
                       }
                
                
                    function SaveProducto(option){
                        //$( "#productForm" ).on('submit', function(event) {
                            event.preventDefault();

                            $("#error").hide();
                            $("#error").empty();

                            var error = false;
                            if(!$("#nombre").val()){
                                error = true;
                                $("#error").append('<label>Ingres un nombre</label><br>');
                            } 
                            if(!$("#costo").val() || $("#costo").val() <= 0){
                                error = true;
                                $("#error").append('<label>Ingrese un costo mayor a 0</label><br>');
                            }
                            if(!$("#stock").val()  || $("#stock").val() <= 0){
                                error = true;
                                $("#error").append('<label>Ingrese un numero de stock mayor a 0</label><br>');
                            }
                            if(!$("#cmbcategory").val()){
                                error = true;
                                $("#error").append('<label>Seleccione una categoria</label><br>');
                            }
                            if(!$("#id").val() && !$("#image").val()){
                                error = true;
                                $("#error").append('<label>Seleccione una imagen</label><br>');
                            }
                            if(error){
                                $("#error").show();
                                return ;
                            }

                            

                            var status = 'PENDIENTE';
                            if(option == 2){
                                status = 'ACEPTADO';
                            }
                            if(option == 3){
                                status = 'RECHAZADO';
                            }

                            var comentario = $("#comentario").val(),
                            comentario = comentario ? comentario : '';

                            var formdata = new FormData();
                            formdata.append('id', $("#id").val());
                            formdata.append('nombre', $("#nombre").val());
                            formdata.append('descripcion', $("#descripcion").val());
                            formdata.append('costo', $("#costo").val());
                            formdata.append('stock', $("#stock").val());
                            formdata.append('id_categoria', $("#cmbcategory").val());
                            formdata.append('comentario',comentario);
                            formdata.append('status',status);
                            formdata.append('image', document.querySelector("#image").files[0]);

                            $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url: "{{url('saveProduct')}}",
                                method:"POST",
                                data: formdata,
                                dataType:'JSON',
                                contentType: false,
                                cache: false,
                                processData: false,
                                success:function(data){
                                    if(data && data.error){
                                        $("#error").show();
                                        for(var i = 0; i<data.error.length; i++){
                                            $("#error").append('<label>'+data.error[i]+'</label><br>');
                                        }

                                    }else{ 
                                        listaproductos();
                                        $("#listaproductos").show();
                                        $("#formProductos").hide();
                                    }
                                   
                                },
                                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                                    console.log("Status: " + textStatus);
                                } 
                            })
                       //});
                    }
                       



                </script>
                


@endsection
