
@extends('layouts.menu')

@section('dashboard')
 



                <!-- Begin Page Content -->
               
                <div class="container-fluid">
                    <p>
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseventa" aria-expanded="false" aria-controls="collapseventa">
                           Ver lista de compra
                        </button>
                    </p>
                    <div class="collapse" id="collapseventa">
                        <div class="card card-body">
                            <div id="listaavender">
                                <div class="card shadow">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table" id="tableToSales" width="100%" cellspacing="0">
                                                <thead>
                                                    <tr>
                                                        <th>Nombre</th>
                                                        <th>Descripcion</th>
                                                        <th>Cantidad</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <button class="btn btn-warning" type="button" id="salesCompleted">
                                                Completar compra
                                             </button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                    </div>

                    
                   
                   
                    <div id="listacateogoria">
                     <div class="card shadow mb-4">
                         <div class="card-body">
                            <nav class="navbar navbar-light bg-light">
                                <form class="form-inline" id="searchForm">
                                  <input class="form-control mr-sm-2" type="search" id="valor_buscar" placeholder="Buscar producto" aria-label="Buscar producto">
                                  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>&nbsp;&nbsp;
                                  <label for="exampleFormControlSelect1">Cliente: &nbsp;&nbsp;&nbsp;&nbsp;</label>
                                   <select class="form-control" style="width: 400px;" id="cmbclientes" name="id_categoria">
                                   </select>
                                </form>
                              </nav>
                             <div class="table-responsive">
                                 <table class="table table-bordered" id="lst_product_result" width="100%" cellspacing="0">
                                     <thead>
                                         <tr>
                                            <th>Imagen</th>
                                             <th>Nombre</th>
                                             <th>Descripcion</th>
                                             <th>Costo</th>
                                             <th>Cantidad a comprar</th>
                                             <th colspan="2" style="width: 2%;">Acciones</th>
                                         </tr>
                                     </thead>
                                     <tbody>
                                     </tbody>
                                 </table>
                             </div>
                         </div>
                     </div>
                   </div>                   



                </div>


   
                <script>
                       //https://sweetalert2.github.io/#download

                    $(document).ready(function(){

                        $("#listacateogoria").show();
                        $("#formCategoria").hide();
                        $("#diverror").hide();



                        listaproductos();
                        listaClients();

                        $( "#searchForm" ).on('submit', function(event) {
                            event.preventDefault();
                            listaproductos();
                        });
                
                        
                        function listaproductos(){
                            $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'{{url('productssearch')}}',
                                method: 'POST',
                                data:{
                                    search: $("#valor_buscar").val()
                                }
                                }).done((res) =>{
                                    let resp = JSON.parse(res);
                                    if(resp){
                                        $("#lst_product_result").find('tbody').empty();                
                                        var data_table = '';
                                        for(var i= 0; i< resp.length; i++){
                                            data_table += "<tr id='table_"+resp[i].id+"'>";                                               
                                            data_table += "<td><img src='/images/"+resp[i].archivo+"' width='130' height='110'></td>";
                                            data_table += "<td id='nombre_"+resp[i].id+"'>"+resp[i].nombre+"</td>";
                                            data_table += "<td>"+resp[i].descripcion+"</td>";
                                            data_table += "<td>"+resp[i].costo+"</td>";
                                            data_table += "<td> <input type='number' min='0' class='form-control' id='monto_"+resp[i].id+"' name='"+resp[i].id+"' placeholder='Cantidad'></td>";
                                            data_table += "<td><button class='btn btn-primary' atr-id='"+resp[i].id+"' style='font-size:12px;' onClick=addProduct(this)><i class='fa fa-plus'></i> Agregar</button>";
                                            data_table += "</tr>";
                                        }
                                        $("#lst_product_result").find('tbody').append(data_table);                
                                }
                                
                                $("#lst_product_result").show();
                            })
                       }
                       
                       function listaClients(){
                            $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'{{url('getAllPerson')}}',
                                method: 'POST',
                                data:{}
                                }).done((res) =>{
                                    let resp = JSON.parse(res);
                                    if(resp){
                                        resp.forEach( function(valor, indice, array) {
                                            $('#cmbclientes').append(                                           
                                                $('<option></option>').val(valor.id).html(valor.nombre + ' ' + valor.paterno + ' ' + valor.materno)
                                            );
                                        });                                      
                                    }
                                })
                       }

                      
                    })//fin de $(document)
                      

                       function addProduct(tr){
                        var id = $( tr ).attr( "atr-id" );
                        var currentRow=$("#table_"+id).closest("tr");                        
                        var nombre_product = currentRow.find("td:eq(1)").text();          
                        var descripcion_product = currentRow.find("td:eq(2)").text();
                        var cantidad = $("#monto_"+id).val();

                        if(!cantidad || cantidad <= 0){
                            Swal.fire( 'Monto', 'Debe ingresar un monto mayor 0.', 'error' );
                        }else{               
                            var data_table = '';
                                data_table += "<tr id='tablesales_"+id+"'  atr-ids='"+id+"' atr-idcantidad='"+cantidad+"'>";            
                                data_table += "<td>"+nombre_product+"</td>";
                                data_table += "<td>"+descripcion_product+"</td>";
                                data_table += "<td>"+cantidad+"</td>";
                                data_table += "<td><button class='btn btn-primary' atr-idsales='"+id+"' style='font-size:12px;' onClick=deleteProductSales(this)><i class='fa fa-trash'></i></button>";
                                data_table += "</tr>";
                            $("#listaavender").find('tbody').append(data_table);      
                            $("#monto_"+id).val('');                            
                            Swal.fire( 'Producto', 'Producto agregado correctamente, revise el listado de compras.', 'success' );
                        }

                       
                       }

                       function deleteProductSales(tr){
                            var id = $( tr ).attr( "atr-idsales" );
                            var currentRow=$("#tablesales_"+id).remove();         
                            Swal.fire( 'Eliminación', 'El producto se ha quitado de la compra.', 'error' );
                       
                       }

                       $("#salesCompleted").click(function() {
                            var id_usuario = $('#cmbclientes option').filter(':selected').val();
                           
                            let details = '[';
                            $('#tableToSales  tbody tr').each(function(index,element){
                                var cantidad = $(element).attr("atr-idcantidad");
                                var id = $(element).attr("atr-ids");
                                details += '{"id":'+id+', "cantidad": '+cantidad+'},';
                            });
                            if(details.length > 1){
                                details = details.substring(0, details.length -1);
                            }

                            details += ']';
                            console.log("details >> ", details)
                            if(details.length <= 4){
                                Swal.fire( 'Venta', 'No ha agregado productos.', 'error' );
                            }else{
                                    $.ajax({
                                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                        url:'{{url('salesCompleted')}}',
                                        data: details,
                                        contentType: "json",
                                        processData: false,
                                        method:'POST'
                                        }).done((res) => { 
                                            let resp = JSON.parse(res);
                                            if(resp){
                                                $.ajax({
                                                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                                    url:'{{url('updateclientsales')}}',
                                                    method: 'POST',
                                                    data:{ id: resp.id, id_usuario: id_usuario }
                                                }).done((ressaless) => {
                                                    if(ressaless){
                                                        Swal.fire( 'Venta', 'Venta creada correctamente.', 'success' );
                                                        $("#listaavender").find('tbody').empty(); 
                                                    
                                                    }else{
                                                        Swal.fire( 'Venta', 'Error al crear la venta.', 'error' );
                                                    }                                 
                                                });
                                            }
                                            else{
                                                Swal.fire( 'Venta', 'Error al crear la venta.', 'error' );
                                            }  
                                            
                                    });
                                
                            }

                            
                            
                            //Swal.fire( 'Compra', 'La compra se ha completado.', 'success' );
                        });
                
                
                </script>
                


@endsection
