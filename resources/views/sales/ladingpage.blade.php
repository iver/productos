@extends('layouts.app')

@section('title', 'Ventas')

@section('content')

<div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Shopping</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav">
            <a class="nav-item nav-link active" href="{{route('login.index')}}">Iniciar sesión <span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link" href="{{route('login.registro')}}">Registrarme</a>
          </div>
        </div>
      </nav>

   <div class="row" style="padding: 30px;">
        @foreach ($productos as $pr)
            <div class="col-md-4 text-center">
                    <div class="card" style="width: 18rem; border:1px; box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);">
                        <img  src="/images/{{$pr->archivo}}" width="130" height="110" style="margin-left:80px;">
                        <br>
                        <hr>
                        <div class="card-body">
                            <h5 class="card-title"><b>{{ $pr->nombre }}</b></h5>
                            <p class="card-text">{{$pr->descripcion}}</p>
                            <a href="#" class="btn btn-link" style="font-size: 18px;"><b>@money($pr->costo)</b></a>
                            <a href="{{route('sales.list')}}" class="btn btn-primary">Comprar</a>
                        </div>
                    </div>
            </div>

        @endforeach


    </div>
</div>




@endsection


<style>
body{
        background-color: rgb(91, 182, 194)!important;
    }
</style>