
@extends('layouts.menu')

@section('dashboard')
 



                <!-- Begin Page Content -->
                <div class="container-fluid">

                   <div id="listaventas">
                     <!-- Page Heading -->
                     <h1 class="h3 mb-2 text-gray-800">Listado de ventas</h1>
                     <div class="card shadow mb-4">
                        <label for="exampleFormControlSelect1" style="margin-left:30px;">Cliente: &nbsp;&nbsp;&nbsp;&nbsp;
                            <select class="form-control" style="width: 400px;" id="cmbclientes" name="id_categoria">
                            </select>
                        </label>
                         <div class="card-body">
                            <div class="table-responsive" style="font-size:15px !important;">
                                 <table class="table table-bordered" id="lstVentas" width="100%" cellspacing="0">
                                     <thead>
                                         <tr>
                                             <th>Venta</th>
                                             <th>Cliente</th>
                                             <th>Total Venta</th>
                                             <th>Comentario</th>
                                             <th>Status Voucher</th>
                                             <th>Comentario Voucher</th>
                                             <th>Status Entrega</th>
                                             <th width="200"; style="width: 30%;">Acciones</th>
                                         </tr>
                                     </thead>
                                     <tbody>                                       
                                        
                                     </tbody>
                                 </table>
                             </div>
                         </div>
                     </div>
                   </div>
                   <div id="formVentas">
                       <form method="post" id="voucherForm" enctype="multipart/form-data">
                            <input type="hidden" class="form-control" id="id" name="id">                            
                            <input type="hidden" class="form-control" id="nombre_imagen" name="nombre_imagen">
                            <div class="form-group">
                                <label for="name">Nombre Cliente</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre Cliente" disabled>
                            </div>
                            <div class="form-group">
                                <label for="name">Venta</label>
                                <input type="text" class="form-control" id="venta" name="nombre" placeholder="Venta" disabled>
                            </div>
                            <div class="form-group">
                                <label for="name">Total Venta</label>
                                <input type="text" class="form-control" id="totalventa" name="totalventa" placeholder="Total Venta" disabled>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                     <table class="table table-bordered" id="lstproductosventas" width="100%" cellspacing="0">
                                         <thead>
                                             <tr>
                                                 <th>Producto</th>
                                                 <th>Cantidad</th>
                                                 <th>Costo</th>
                                             </tr>
                                         </thead>
                                         <tbody>                                       
                                            
                                         </tbody>
                                     </table>
                                 </div>
                             </div>


                            <br>
                            <div class="custom-file" id="divvoucher">
                                <input type="file" class="custom-file-input" id="image" name="image" lang="es">
                                <label class="custom-file-label" for="image">Seleccionar Archivo</label>
                            </div>


                            <div class="row" id="divframeProduct">
                                <div class="col-md-2"></div> 
                                <div class="col-md-6"><img id="frameProduct" width="500" height="400" /></div>
                                <div class="col-md-4"></div>                                
                            </div>


                            


                            <br> <br>

                            <div class="row" style="width: 100%;padding:20px;" id="commentariosp">
                                <div class="form-group" style="width: 100%;padding:20px;">
                                    <label for="name">Comentario / motivo de cancelación</label>
                                    <textarea class="form-control" id="comentariovalidacion" name="comentariovalidacion" rows="3"></textarea>
                                </div>                                
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-info" id="cancelProduct" onclick="editProduct()">Cancelar</button>&nbsp;&nbsp;&nbsp;
                                    
                                    <button type="submit" class="btn btn-success" id="btnValidar" onClick="GuardarVoucher(1)">Validar</button>
                                    <button type="submit" class="btn btn-primary" id="btnGuardar" onClick="GuardarVoucher(2)">Guardar</button>
                                    <button type="submit" class="btn btn-danger" id="btnRechazar" onClick="GuardarVoucher(3)">Rechazar</button>
                                    
                                </div>
                                
                            </div>

                       </form>
                   </div>






                   <!-- Logout Modal-->
                    <div class="modal fade" id="salesComentario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"></h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="post" id="comentarioForm" enctype="multipart/form-data">
                                    <input type="hidden" id="idcomentario" name="idcomentario" />
                                    <div class="form-group">
                                        <label for="name">Comentario</label>
                                        <textarea class="form-control" id="comentarioventa" name="comentarioventa" rows="4"></textarea>
                                    </div>

                                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary" id="savecomentario">Guardar</button>
                                </form>
                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                    </div>
                </div>


                   



                </div>


   
                <script>
                       //https://sweetalert2.github.io/#download

                    $(document).ready(function(){

                        $("#listaventas").show();
                        $("#formVentas").hide();
                        listaClients();


                        function listaClients(){
                            $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'{{url('getAllPerson')}}',
                                method: 'POST',
                                data:{}
                                }).done((res) =>{
                                    let resp = JSON.parse(res);
                                    if(resp){
                                        $('#cmbclientes').append(                                           
                                                $('<option></option>').val('').html('Todos')
                                            );
                                        resp.forEach( function(valor, indice, array) {
                                            $('#cmbclientes').append(                                           
                                                $('<option></option>').val(valor.id).html(valor.nombre + ' ' + valor.paterno + ' ' + valor.materno)
                                            );
                                        });                                      
                                    }
                                })
                       }

                       $('#cmbclientes').on('change', function() {
                            listaventas(this.value);
                       });


                       $( "#addProduct" ).click(function() {
                            $("#id").val('');
                            $("#nombre").val('');
                            $("#descripcion").val('');
                            $("#costo").val(''); 
                            $("#stock").val('');
                            $("#divframeProduct").hide();
                            $("#listaventas").hide();
                            $("#formVentas").show();
                            $('#cmbcategory').empty();

                            $.ajax({
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'{{url('allcat')}}',
                                method: 'POST',
                                data:{ }
                            }).done((res) =>{
                                $('#cmbcategory').empty();
                                let resp = JSON.parse(res);
                                if(resp && resp.category){
                                    resp.category.forEach( function(valor, indice, array) {
                                        $('#cmbcategory').append(                                           
                                            $('<option></option>').val(valor.id).html(valor.nombre)
                                        );
                                    });
                                }
                            })

                       });
                
                       $( "#cancelProduct" ).click(function() {
                            $("#listaventas").show();
                            $("#formVentas").hide();
                       });
                
                       listaventas('');
                
                       function listaventas(id_usuario){
                            $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'/sales/all',
                                method: 'POST',
                                data:{id_usuario: id_usuario}
                                }).done((res) =>{
                                    let respons = JSON.parse(res);
                                    resp = respons.results;
                                    console.log("venta > ", respons)
                                    if(resp){
                                        $("#lstVentas").find('tbody').empty();                
                                        var data_table = '';
                                        for(var i= 0; i< resp.length; i++){
                                            data_table += "<tr>";
                                            data_table += "<td> VENTA "+resp[i].fecha_compra+"</td>";
                                            data_table += "<td>"+resp[i].nombre + ' ' + resp[i].paterno + ' ' + resp[i].materno+"</td>";
                                            data_table += "<td>$"+(resp[i].total ? parseFloat(resp[i].total).toFixed(2) : 0.0)+"</td>";
                                            data_table += "<td>"+resp[i].comentario +"<button type='button'  onClick=changeComment("+resp[i].id+") class='btn btn-link'>Cambiar comentario</button></td>";
                                            data_table += "<td>";
                                            if(resp[i].status_voucher == 'PENDIENTE'){
                                                data_table += "<button class='btn btn-warning' style='font-size:10px;'>PENDIENTE</button>";                                                
                                            }if(resp[i].status_voucher == 'ACEPTADO'){                                                
                                                data_table += "<button class='btn btn-success' style='font-size:10px;'>ACEPTADO</button>";
                                            }if(resp[i].status_voucher == 'RECHAZADO'){              
                                                data_table += "<button class='btn btn-danger' style='font-size:10px;'>RECHAZADO</button>"; 
                                            }
                                            data_table += "</td>";

                                            data_table += "<td>"+resp[i].motivo_validacion +"</td>";
                                            data_table += '<td>'
                                            if(resp[i].status == 'PENDIENTE'){
                                                data_table += '<button class="btn btn-success" style="font-size:10px;">POR ENTREGAR</button>';
                                            }else{
                                                data_table += '<button class="btn btn-warning" style="font-size:10px;" onClick=addVoucher('+resp[i].id+') >ENTREGADO</button>';
                                            }
                                            data_table += '</td>';

                                            data_table += '<td>'
                                            if(respons.voucher){
                                                data_table += '<button class="btn btn-primary" style="font-size:10px;" onClick=addVoucher('+resp[i].id+') >Voucher</button>';
                                            }
                                          
                                            data_table += '&nbsp;<button class="btn btn-info" style="font-size:10px;" onClick=verificarVoucher('+resp[i].id+') >Ver detalle</button>';
 
                                            if(respons.verificarvoucher){
                                                data_table += '&nbsp;<button class="btn btn-primary" style="font-size:10px;" onClick=verificarVoucher('+resp[i].id+') >Verificar</button>';
                                            }    
                                            if(respons.validarentrega && resp[i].status == 'PENDIENTE'){
                                                data_table += '&nbsp;<button class="btn btn-danger" style="font-size:10px;" onClick=entregarProducto('+resp[i].id+') >Entregar</button>';
                                            }
                                            if(respons.validarentrega && resp[i].status != 'PENDIENTE'){
                                                data_table += '&nbsp;<button class="btn btn-danger" style="font-size:10px;" onClick=entregarProducto('+resp[i].id+') >Revertir entrega</button>';
                                            }  
  
                                            data_table += '</td>';

                                           
                                            
                                        }
                                        $("#lstVentas").find('tbody').append(data_table);                
                                }
                                
                                $("#listaventas").show();
                                $("#formVentas").hide();
                            })
                       }
                
                
                       /*$( "#voucherForm" ).on('submit', function(event) {
                            event.preventDefault();
                            $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url: "{{url('saveVoucher')}}",
                                method:"POST",
                                data:new FormData(this),
                                dataType:'JSON',
                                contentType: false,
                                cache: false,
                                processData: false,
                                success:function(data){
                                    listaventas('');
                                    $("#listaventas").show();
                                    $("#formVentas").hide();
                                },
                                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                                    console.log("Status: " + textStatus);
                                } 
                            })
                       });*/
                       
                
                       function changeComment(id, Comentario){
                        console.log("id > ", id)
                        console.log("Comentario > ", Comentario)
                       }
                
                      
                    })//fin de $(document)

                        function changeComment(id, Comentario){
                            $('#salesComentario').modal('show');//muestra modal
                            $("#idcomentario").val(id);
                            $("#comentarioventa").val('');

                            $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'/salesunique/id',
                                method: 'POST',
                                data:{id: id}
                                }).done((res) =>{
                                    let respons = JSON.parse(res);
                                    if(respons && respons.sales){
                                        $("#comentarioventa").val(respons.sales.comentario);
                                    }
                           
                                })


                       }

                       
                       $( "#comentarioForm" ).on('submit', function(event) {
                            event.preventDefault();
                            console.log("enviadn comentario")
                            var comentario = $("#comentarioventa").val();
                            var id = $("#idcomentario").val();
                            $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'{{url('saveComentarioSales')}}',
                                method: 'POST',
                                data:{ id:id, comentario:comentario ? comentario : '' },
                                success:function(data){
                                    $('#salesComentario').modal('hide');//muestra modal
                                    //Swal.fire( 'Comentario', 'Comentario actualizado correctamente.', 'success' );
                                    listaventas($("#cmbclientes").val());
                                },
                                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                                    console.log("Status: " + textStatus);
                                } 
                            })
                       });


                        function addVoucher(id){    
                            $("#commentariosp").hide();
                            $("#btnValidar").hide();
                            $("#btnRechazar").hide();
                            $("#btnGuardar").show();
                            $("#divvoucher").show();

                            $("#listaventas").hide();
                            $("#formVentas").show();
                            
                            var iframe = document.getElementById("frameProduct");
                            iframe.setAttribute("src", "/images/");
                            
                            $("#lstproductosventas").find('tbody').empty();     

                            $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'/sales/id',
                                method: 'POST',
                                data:{id: id}
                                }).done((res) =>{
                                    let respons = JSON.parse(res);
                                    console.log(respons);

                                    if(respons && respons.sales.length > 0){
                                        $("#listaventas").hide();
                                        $("#formVentas").show(); 

                                        $("#nombre").val(respons.sales[0].nombre + ' ' + respons.sales[0].paterno + ' ' + respons.sales[0].materno );
                                        $("#venta").val(respons.sales[0].nombreventa);
                                        $("#totalventa").val(respons.sales[0].total);
                                        $("#id").val(respons.sales[0].id);
                                    }                                   

                                    if(respons && respons.productos.length > 0){
                                        var data_table = '';                                          
                                        for(var i= 0; i< respons.productos.length; i++){
                                            data_table += "<tr>";
                                            data_table += "<td>"+respons.productos[i].nombre+"</td>";
                                            data_table += "<td>"+respons.productos[i].cantidad+"</td>";
                                            data_table += "<td>"+respons.productos[i].costo+"</td>";
                                           data_table += "</tr>";
                                        }
                                        $("#lstproductosventas").find('tbody').append(data_table);  
                                    }

                                    if(respons && respons.files.length > 0){
                                        $("#divframeProduct").show();
                                        var iframe = document.getElementById("frameProduct");
                                        iframe.setAttribute("src", "/images/"+respons.files[0].archivo);
                                    }
                                    
                                   
                                });
                                


                       }
                       

                       
                       function verificarVoucher(id){   
                            $("#btnValidar").show();
                            $("#btnRechazar").show();
                            $("#btnGuardar").hide();   
                            $("#divvoucher").hide();
                            $("#comentariovalidacion").val('');

                            var iframe = document.getElementById("frameProduct");
                            iframe.setAttribute("src", "/images/");
                                        
                            $("#listaventas").hide();
                            $("#formVentas").show();
                            
                            $("#lstproductosventas").find('tbody').empty();     

                            $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'/sales/id',
                                method: 'POST',
                                data:{id: id}
                                }).done((res) =>{
                                    let respons = JSON.parse(res);
                                    console.log(respons);
                                    $("#divframeProduct").show();
                                    var iframe = document.getElementById("frameProduct");
                                    iframe.setAttribute("src", "/images/");

                                    if(respons && respons.sales.length > 0){
                                        $("#listaventas").hide();
                                        $("#formVentas").show(); 

                                        $("#nombre").val(respons.sales[0].nombre + ' ' + respons.sales[0].paterno + ' ' + respons.sales[0].materno );
                                        $("#venta").val(respons.sales[0].nombreventa);
                                        $("#totalventa").val(respons.sales[0].total);
                                        $("#id").val(respons.sales[0].id);
                                    }

                                    if(respons && respons.productos.length > 0){
                                        var data_table = '';                                          
                                        for(var i= 0; i< respons.productos.length; i++){
                                            data_table += "<tr>";
                                            data_table += "<td>"+respons.productos[i].nombre+"</td>";
                                            data_table += "<td>"+respons.productos[i].cantidad+"</td>";
                                            data_table += "<td>"+respons.productos[i].costo+"</td>";
                                           data_table += "</tr>";
                                        }
                                        $("#lstproductosventas").find('tbody').append(data_table);  
                                    }

                                    if(respons && respons.files.length > 0){
                                        $("#divframeProduct").show();
                                        var iframe = document.getElementById("frameProduct");
                                        iframe.setAttribute("src", "/images/"+respons.files[0].archivo);
                                    }
                                    
                                   
                                });
                                


                       }

                       function entregarProducto(id){
                        $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'/sales/changestatus',
                                method: 'POST',
                                data:{id: id}
                                }).done((res) =>{
                                    let respons = JSON.parse(res);                                    
                                    if(respons){
                                        listaventas($("#cmbclientes").val());
                                        Swal.fire( 'Entrega', 'Estado actualizado correctamente.', 'success' );
                                    }else{                                        
                                        Swal.fire( 'Entrega', 'Ocurrio un error', 'error' );
                                    }
                                });
                                


                       }
                       
                       function listaventas(id_usuario){
                            $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'/sales/all',
                                method: 'POST',
                                data:{id_usuario: id_usuario}
                                }).done((res) =>{
                                    let respons = JSON.parse(res);
                                    resp = respons.results;
                                    console.log("venta > ", resp)
                                    if(resp){
                                        $("#lstVentas").find('tbody').empty();                
                                        var data_table = '';
                                        for(var i= 0; i< resp.length; i++){
                                            data_table += "<tr>";
                                            data_table += "<td> VENTA "+resp[i].fecha_compra+"</td>";
                                            data_table += "<td>"+resp[i].nombre + ' ' + resp[i].paterno + ' ' + resp[i].materno+"</td>";
                                            data_table += "<td>$"+(resp[i].total ? parseFloat(resp[i].total).toFixed(2) : 0.0)+"</td>";
                                            data_table += "<td>"+resp[i].comentario +"<button type='button'  onClick=changeComment("+resp[i].id+") class='btn btn-link'>Cambiar comentario</button></td>";
                                            data_table += "<td>";
                                            if(resp[i].status_voucher == 'PENDIENTE'){
                                                data_table += "<button class='btn btn-warning' style='font-size:10px;'>PENDIENTE</button>";                                                
                                            }if(resp[i].status_voucher == 'ACEPTADO'){                                                
                                                data_table += "<button class='btn btn-success' style='font-size:10px;'>ACEPTADO</button>";
                                            }if(resp[i].status_voucher == 'RECHAZADO'){              
                                                data_table += "<button class='btn btn-danger' style='font-size:10px;'>RECHAZADO</button>";
                                            }
                                            data_table += "</td>";

                                            data_table += "<td>"+resp[i].motivo_validacion +"</td>";

                                            data_table += '<td>'
                                            if(resp[i].status == 'PENDIENTE'){
                                                data_table += '<button class="btn btn-warning" style="font-size:10px;">POR ENTREGAR</button>';
                                            }else{
                                                data_table += '<button class="btn btn-success" style="font-size:10px;" onClick=addVoucher('+resp[i].id+') >ENTREGADO</button>';
                                            }
                                            data_table += '</td>';

                                            data_table += '<td>'
                                            if(respons.voucher){
                                                data_table += '<button class="btn btn-primary" style="font-size:10px;" onClick=addVoucher('+resp[i].id+') >Voucher</button>';
                                            }
                                          
                                            data_table += '&nbsp;<button class="btn btn-info" style="font-size:10px;" onClick=verificarVoucher('+resp[i].id+') >Ver detalle</button>';
 
                                            if(respons.verificarvoucher){
                                                data_table += '&nbsp;<button class="btn btn-primary" style="font-size:10px;" onClick=verificarVoucher('+resp[i].id+') >Verificar</button>';
                                            }    
                                            if(respons.validarentrega && resp[i].status == 'PENDIENTE'){
                                                data_table += '&nbsp;<button class="btn btn-danger" style="font-size:10px;" onClick=entregarProducto('+resp[i].id+') >Entregar</button>';
                                            }
                                            if(respons.validarentrega && resp[i].status != 'PENDIENTE'){
                                                data_table += '&nbsp;<button class="btn btn-danger" style="font-size:10px;" onClick=entregarProducto('+resp[i].id+') >Revertir entrega</button>';
                                            }   
  
                                            data_table += '</td>';

                                           
                                            
                                        }
                                        $("#lstVentas").find('tbody').append(data_table);                
                                }
                                
                                $("#listaventas").show();
                                $("#formVentas").hide();
                            })
                       }

                       function GuardarVoucher(option){
                            event.preventDefault();
                            
                            var comentariovalidacion = $("#comentariovalidacion").val();
                            comentariovalidacion = comentariovalidacion ? comentariovalidacion : '';

                            var status = option == 1 ? 'ACEPTADO' : 'RECHAZADO';

                            if(option == 1 || option == 3){
                                $.ajax({
                                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                    url: "{{url('saveValidation')}}",
                                    method:"POST",
                                    data:{id: $("#id").val(), comentariovalidacion: comentariovalidacion, status: status},
                                    success:function(data){
                                        listaventas('');
                                        $("#listaventas").show();
                                        $("#formVentas").hide();
                                    },
                                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                                        console.log("Status: " + textStatus);
                                    } 
                                })
                           }
                           if(option == 2){
                            var formdata = new FormData();

                                // textual parameters
                                formdata.append('id', $("#id").val());
                                formdata.append('image', document.querySelector("#image").files[0]);
        
                                $.ajax({
                                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                    url: "{{url('saveVoucher')}}",
                                    method:"POST",
                                    data: formdata,//new FormData(this),
                                    dataType:'JSON',
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    success:function(data){
                                        $("#listaventas").show();
                                        $("#formVentas").hide();
                                    },
                                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                                        console.log("Status: " + textStatus);
                                    } 
                                })
                           }
                       }
                
                
                </script>
                


@endsection
