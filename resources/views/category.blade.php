
@extends('layouts.menu')

@section('dashboard')
 



                <!-- Begin Page Content -->
                <div class="container-fluid">

                   <div id="listacateogoria">
                     <!-- Page Heading -->
                     <h1 class="h3 mb-2 text-gray-800">Listado de categorias</h1>  
                     <div class="card shadow mb-4">
                         <div class="card-body">
                            <div class="float-right"> <button type="button" class="btn btn-primary" id="addCategory">Registrar</button> <br></div>
                             <div class="table-responsive">
                                 <table class="table table-bordered" id="lstCategory" width="100%" cellspacing="0">
                                     <thead>
                                         <tr>
                                             <th>Nombre</th>
                                             <th>Descripcion</th>
                                             <th colspan="2" style="width: 2%;">Acciones</th>
                                         </tr>
                                     </thead>
                                     <tbody>
                                     </tbody>
                                 </table>
                             </div>
                         </div>
                     </div>
                   </div>
                   <div id="formCategoria">
                      <div class="alert alert-danger" role="alert" id="error">
                        
                      </div>
                       <form method="post" id="categoryForm" enctype="multipart/form-data">
                            <input type="hidden" class="form-control" id="id" name="id">
                            <div class="form-group">
                                <label for="name">Nombre de la categoria</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre de la categoria">
                            </div>
                            <div class="form-group">
                                <label for="name">Descripción</label>
                                <textarea class="form-control" id="descripcion" name="descripcion" rows="3"></textarea>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image" name="image" lang="es">
                                <label class="custom-file-label" for="image">Seleccionar Archivo</label>
                            </div>

                            <div class="row" id="divframeCategory">
                                <div class="col-md-2"></div> 
                                <div class="col-md-6"><img id="frameCategory" width="500" height="400" /></div>
                                <div class="col-md-4"></div>                                
                            </div>

                            <br> <br>
                            
                            <div class="row">
                                <div class="col-md-9">
                                </div>
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-info" id="cancelCategory" onclick="editCategory()">Cancelar</button>&nbsp;&nbsp;&nbsp;
                                    <button type="submit" class="btn btn-primary" id="saveCategory">Guardar</button>
                                </div>
                                
                            </div>

                       </form>
                   </div>





                   



                </div>


   
                <script>
                       //https://sweetalert2.github.io/#download

                    $(document).ready(function(){

                        $("#listacateogoria").show();
                        $("#formCategoria").hide();
                        $("#diverror").hide();
                        $("#error").hide();
                
                       $( "#addCategory" ).click(function() {
                            $("#id").val('');
                            $("#nombre").val('');
                            $("#descripcion").val('');
                            $("#error").hide();

                            $("#divframeCategory").hide();
                            $("#listacateogoria").hide();
                            $("#formCategoria").show();

                       });
                
                       $( "#cancelCategory" ).click(function() {
                            $("#listacateogoria").show();
                            $("#formCategoria").hide();
                       });
                
                       listacateogoria();
                
                       function listacateogoria(){
                            $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'/category/all',
                                method: 'POST',
                                data:{}
                                }).done((res) =>{
                                    let resp = JSON.parse(res);
                                    if(resp){
                                        $("#lstCategory").find('tbody').empty();                
                                        var data_table = '';
                                        for(var i= 0; i< resp.length; i++){
                                            data_table += "<tr>";
                                            data_table += "<td>"+resp[i].nombre+"</td>";
                                            data_table += "<td>"+resp[i].descripcion+"</td>";
                                            data_table += '<td><button class="btn btn-primary" style="font-size:12px;" onClick=editCategory('+resp[i].id+')><i class="fa fa-edit"></i></button>';
                                            data_table += '<td><button class="btn btn-danger" style="font-size:12px;" onClick=deleteCategory('+resp[i].id+') ><i class="fa fa-trash"></i></button>';
                                            data_table += "</tr>";
                                        }
                                        $("#lstCategory").find('tbody').append(data_table);                
                                }
                                
                                $("#listacateogoria").show();
                                $("#formCategoria").hide();
                            })
                       }
                
                
                       $( "#categoryForm" ).on('submit', function(event) {
                            event.preventDefault();

                            $("#error").hide();
                            $("#error").empty();

                            var error = false;
                            if(!$("#nombre").val()){
                                error = true;
                                $("#error").append("<label>Ingrese un nombre de categoria</label><br>");
                            }

                            if(!$("#image").val()){ 
                                error = true;
                                $("#error").append("<label>Seleccione una imagen</label><br>");
                            }

                            if(error){
                                $("#error").show();
                                return ;
                            }

                            
                            $("#diverror").empty();
                            $("#diverror").hide();
                            
                            $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url: "{{url('savecategory')}}",
                                method:"POST",
                                data:new FormData(this),
                                dataType:'JSON',
                                contentType: false,
                                cache: false,
                                processData: false,
                                success:function(data){
                                    listacateogoria();
                                    $("#listacateogoria").show();
                                    $("#formCategoria").hide();
                                },
                                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                                    console.log("Status: " + textStatus);
                                } 
                            })
                       });
                       
                
                
                
                      
                    })//fin de $(document)
                
                    function deleteCategory(id){                       
                            Swal.fire({
                                title: 'Eliminación',
                                text: "¿Esta seguro de eliminar el registro?",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Eliminar'
                            }).then( 
                                function (res) {
                                    if(res.isConfirmed)
                                        deleteCategoryItem(id);
                                },
                            );
                       }
                       

                       function deleteCategoryItem(id){
                            $.ajax({
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'{{url('deleteCategory')}}',
                                method: 'POST',
                                data:{ id:id }
                            }).done((res) =>{
                                Swal.fire( 'Eliminación!', 'Registro eliminado correctamente.', 'success' )
                                listacateogoria();
                                $("#listacateogoria").show();
                                $("#formCategoria").hide();  
                            
                            })
                       }
                
                       function editCategory(id){
                            $("#listacateogoria").hide();
                            $("#formCategoria").show();

                            $.ajax({
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                url:'{{url('editcategory')}}',
                                method: 'POST',
                                data:{ id:id }
                            }).done((res) =>{
                                let resp = JSON.parse(res);
                                if(resp && resp.category){
                                    $("#id").val(resp.category.id);
                                    $("#nombre").val(resp.category.nombre);
                                    $("#descripcion").val(resp.category.descripcion);

                                    $("#listacateogoria").hide();
                                    $("#formCategoria").show();
                                }

                                
                                if(resp && resp.file){
                                    $("#divframeCategory").show();
                                    var iframe = document.getElementById("frameCategory");
                                    iframe.setAttribute("src", "/images/"+resp.file.archivo);
                                }
                            })
                       }


                       
                       function listacateogoria(){
                            $.ajax({
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                url:'/category/all',
                                method: 'POST',
                                data:{}
                            }).done((res) =>{
                                let resp = JSON.parse(res);
                                if(resp){
                                    $("#lstCategory").find('tbody').empty();
                                    var data_table = '';
                                    for(var i= 0; i< resp.length; i++){
                                        data_table += "<tr>";
                                        data_table += "<td>"+resp[i].nombre+"</td>";
                                        data_table += "<td>"+resp[i].descripcion+"</td>";
                                        data_table += '<td><button class="btn btn-primary" style="font-size:12px;" onClick=editCategory('+resp[i].id+')><i class="fa fa-edit"></i></button>';
                                        data_table += '<td><button class="btn btn-danger" style="font-size:12px;" onClick=deleteCategory('+resp[i].id+') ><i class="fa fa-trash"></i></button>';
                                        data_table += "</tr>";
                                    }
                                    $("#lstCategory").find('tbody').append(data_table);
                                }
                                $("#listacateogoria").show();
                                $("#formCategoria").hide();
                            })
                       }
                
                
                </script>
                


@endsection
