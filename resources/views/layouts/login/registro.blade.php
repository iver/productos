@extends('layouts.app')

@section('title', 'Ventas')

@section('content')


<div class="login-form">
    {!! Form::open(['url' => '/register','enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
          <label for="exampleInputEmail1">Nombre(s)</label>
          {!! Form::text('nombre', '', ['class' => 'form-control', 'required', 'placeholder' => 'Ingresa tu nombre']) !!}
          @if($errors->has('nombre'))
                <div class="error" style="font-size:10px; color: red;">{{ $errors->first('nombre') }}</div>
          @endif
        </div>
        <div class="form-group">
            <label for="paterno">Apellido Paterno</label>
            {!! Form::text('paterno', null, ['class' => 'form-control', 'required', 'placeholder' => 'Ingresa tu apellido paterno']) !!}
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Apellido Materno</label>
            {!! Form::text('materno', null, ['class' => 'form-control', 'required', 'placeholder' => 'Ingresa tu apellido materno']) !!}
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Correo electronico</label>
            {!! Form::text('email', null, ['class' => 'form-control', 'required', 'placeholder' => 'Ingresa tu email']) !!}
            @if($errors->has('email'))
                <div class="error" style="font-size:10px; color: red;">{{ $errors->first('email') }}</div>
            @endif
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Contraseña</label>
            {!! Form::password('password', ['class' => 'form-control', 'required', 'placeholder' => 'Ingresa tu contraseña']) !!}
            @if($errors->has('password'))
                <div class="error" style="font-size:10px;color: red;">{{ $errors->first('password') }}</div>
            @endif
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Confirmar contraseña</label>
          {!! Form::password('confirmationp', ['class' => 'form-control', 'required', 'placeholder' => 'Ingresa al confirmación de contraseña']) !!}
          @if($errors->has('confirmationp'))
                <div class="error" style="font-size:10px;color: red;">{{ $errors->first('confirmationp') }}</div>
          @endif
        </div>
        <div class="row" style="padding-left:20px;padding-right:20px;">
            {!! Form::submit('Registrarme', ['class' => 'btn btn-primary']) !!}
        </div>
        <div class="row">
            <div class="col-md-10"></div>
            <div class="col-md-2">
                <a href="{{route('login.index')}}" class="btn btn-link">Login</a>
            </div>
        </div>
        {!! Form::close() !!}
</div>
@endsection

<style>
    .error{
        color: red;
    }

    .login-form {
        width: 540px;
        margin: 50px auto;
          font-size: 15px;
    }
    .login-form form {
        margin-bottom: 15px;
        height: 620px;
        background: white;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }

    body{
        background: #96b5ba !important;
        background-color: gray!important;
        background-image: -webkit-linear-gradient(top left, #4097ca, #a26199)!important;;
        color: rgb(218, 188, 188);
    }
    </style>