@extends('layouts.app')

@section('title', 'Ventas')

@section('content')


<div class="login-form">
    {!! Form::open(['url' => '/singin','enctype' => 'multipart/form-data']) !!}
        
        <h2 class="text-center" style="color:#508ec2;">Iniciar Sesión</h2> 
        <br>
        <br>      
        @if(session()->has('message'))
            <div class="alert alert-warning alert-dismissible fade show">
                <strong></strong> {{ session()->get('message') }}
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alert alert-warning alert-dismissible fade show">
                <strong></strong> {{ session()->get('error') }}
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        @endif
        
        <div class="form-group">
            {!! Form::text('email', '', ['class' => 'form-control', 'required', 'placeholder' => 'Ingresa tu email']) !!}
        </div>
        <br>
        <div class="form-group">
            {!! Form::password('password', ['class' => 'form-control', 'required', 'placeholder' => 'Ingresa tu contraseña']) !!}
        </div>
        <div class="form-group pull-right" >
                <button type="block" class="btn btn-primary btn-block">Log in</button>          
        </div>
        <br><br>
        <div class="row">
            <div class="col-md-10"></div>
            <div class="col-md-2">
                <a href="{{route('login.registro')}}" class="btn btn-link">Registrar</a>
            </div>
        </div>        
    {!! Form::close() !!}
</div>
@endsection

<style>
    .login-form {
        width: 540px;
        margin: 50px auto;
          font-size: 15px;
    }
    .login-form form {
        margin-bottom: 15px;
        height: 400px;
        background: white;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }

    body{
        background: #96b5ba !important;
        background-color: gray!important;
        background-image: -webkit-linear-gradient(top left, #4097ca, #a26199)!important;;
        color: rgb(218, 188, 188);
    }
    </style>