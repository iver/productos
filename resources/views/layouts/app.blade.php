<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">

    <link rel="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    
    {!! Html::style('template/css/sb-admin-2.min.css')  !!}
    {!! Html::script('template/js/sb-admin-2.min.js')  !!}
    {!! Html::style('font/all.min.css')  !!}
    {!! Html::style('font/font-awesome.min.css')  !!}
    {!! Html::style('font/brands.min.css')  !!}
    {!! Html::style('font/fontawesome.min.css')  !!}
    {!! Html::style('font/regular.min.css')  !!}
    {!! Html::style('font/svg-with-js.min.css')  !!}
    {!! Html::style('font/svg-with-js.min.css')  !!}
    {!! Html::style('font/v4-shims.min.css')  !!}

    
    {!! Html::script('font/all.min.js')  !!}
    {!! Html::script('font/font-awesome.min.js')  !!}
    {!! Html::script('font/brands.min.js')  !!}
    {!! Html::script('font/fontawesome.min.js')  !!}
    {!! Html::script('font/regular.min.js')  !!}
    {!! Html::script('font/svg-with-js.min.js')  !!}
    {!! Html::script('font/svg-with-js.min.js')  !!}
    {!! Html::script('font/v4-shims.min.js')  !!}
    
    <title>Document</title>
</head>
<body>
        @yield('content')
</body>
</html>