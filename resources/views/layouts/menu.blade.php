@extends('layouts.app')

@section('title', 'Login')

@section('content')


<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
            <div class="sidebar-brand-icon rotate-n-15">
            </div>
            <div class="sidebar-brand-text mx-3">Productos <sup></sup></div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">


        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Menú
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
                @if(Auth::user()->code_role == 'NNM') 
                <a class="nav-link collapsed" href="#">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Productos</span>
                </a>
                @endif
                @if(Auth::user()->code_role == 'SPRVSR' || Auth::user()->code_role == 'CMPRDR' || Auth::user()->code_role == 'NCRGD' || Auth::user()->code_role == 'CNTDR') 
                <a class="nav-link collapsed" href="{{route('producto.list')}}">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Buscar productos</span>
                </a>
                @endif

                @if(Auth::user()->code_role == 'NCRGD') 
                <a class="nav-link collapsed" href="{{route('sales.liststatus')}}">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Consignar un producto</span>
                </a>
                @endif
                
                @if(Auth::user()->code_role == 'CMPRDR' || Auth::user()->code_role == 'VNDDR' || Auth::user()->code_role == 'SPRVSR' || Auth::user()->code_role == 'CNTDR')
                <a class="nav-link collapsed" href="{{route('sales.liststatus')}}">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Ventas / Pagos</span>
                </a>
                @endif


        
            @if(Auth::user()->code_role == 'SPRVSR' || Auth::user()->code_role == 'NCRGD')
            <a class="nav-link collapsed" href="{{route('category.list')}}">
                <i class="fas fa-fw fa-cog"></i>
                <span>Categorías</span>
            </a>
            @endif
            @if(Auth::user()->code_role == 'CMPRDR'|| Auth::user()->code_role == 'VNDDR') 
            <a class="nav-link collapsed" href="{{route('sales.list')}}">
                <i class="fas fa-fw fa-cog"></i>
                <span>Compra producto</span>
            </a>
            @endif
            @if(Auth::user()->code_role == 'SPRVSR')
            <a class="nav-link collapsed"  href="{{route('user.list')}}">
                <i class="fas fa-fw fa-cog"></i>
                <span>Adminitración usuarios</span>
            </a>
            @endif

            @if(Auth::user()->code_role == 'SPRVSR') 
            <a class="nav-link collapsed" href="#">
                <i class="fas fa-fw fa-cog"></i>
                <span>Kardex de productos</span>
            </a>
            @endif
           



            @if(Auth::user()->code_role == 'CNTDR') 
            <a class="nav-link collapsed" href="#">
                <i class="fas fa-fw fa-cog"></i>
                <span>Crear un nuevo pago</span>
            </a>
            @endif
            

        </li>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">


                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">



                    <div class="topbar-divider d-none d-sm-block"></div>

                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{Auth::user()->email}}</span>
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                            aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                Cerrar Sesión
                            </a>
                        </div>
                    </li>

                </ul>

            </nav>
            <!-- End of Topbar -->




            @yield('dashboard')


            


                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->


    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">¿Realmente Desea salir?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <a class="btn btn-primary" href="{{ url("/logout") }}">Salir</a>
                </div>
            </div>
        </div>
    </div>

    







    @endsection