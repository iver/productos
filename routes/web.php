<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\loginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\productController;
use App\Http\Controllers\categoryController;
use App\Http\Controllers\salesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('sales.ladingpage');
});*/


//Route::get('/login', 'loginController@login');


Route::get('/login', [loginController::class, 'login'])->name('login.index');
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/', [HomeController::class, 'products'])->name('products');

Route::get('/logout', [loginController::class, 'signOut'])->name('signout');
Route::post('/singin', 'loginController@singin')->name('singin');


Route::get('/registro', [RegisterController::class, 'registro'])->name('login.registro');
Route::get('registration', [RegisterController::class, 'registration'])->name('register-user');
Route::post('custom-registration', [RegisterController::class, 'customRegistration'])->name('register.custom');
Route::post('/register', 'RegisterController@postRegister')->name('register');

Route::get('/user/lista', [RegisterController::class, 'lista'])->name('user.list');
Route::post('/user/all', 'RegisterController@all');
Route::post('saveUser', [RegisterController::class,'saveUser'])->name('saveUser');
Route::post('editUser', [RegisterController::class,'editUser'])->name('editUser');
Route::post('deleteUser', [RegisterController::class,'deleteUser'])->name('deleteUser');
Route::post('allroles', [RegisterController::class,'allroles'])->name('allroles');



//registro de productos

Route::get('/product/lista', [productController::class, 'lista'])->name('producto.list');
Route::post('/product/all', 'productController@all');
Route::post('saveProduct', [productController::class,'saveProduct'])->name('saveProduct');
Route::post('editProduct', [productController::class,'editProduct'])->name('editProduct');
Route::post('deleteProduct', [productController::class,'deleteProduct'])->name('deleteProduct');


Route::get('/category/lista', [categoryController::class, 'lista'])->name('category.list');
Route::post('/category/all', 'categoryController@all');
Route::post('savecategory', [categoryController::class,'savecategory'])->name('savecategory');
Route::post('editcategory', [categoryController::class,'editcategory'])->name('editcategory');
Route::post('deleteCategory', [categoryController::class,'deleteCategory'])->name('deleteCategory');
Route::post('allcat', [categoryController::class,'allcat'])->name('allcat');



Route::get('/sales', [salesController::class, 'lista'])->name('sales.list');
Route::get('/sales/list/status', [salesController::class, 'liststatus'])->name('sales.liststatus');
Route::post('/sales/all', 'salesController@allsales');
Route::post('/sales/id', 'salesController@salesid');
Route::post('/salesunique/id', 'salesController@salesunique');
Route::post('saveVoucher', [salesController::class,'saveVoucher'])->name('saveVoucher');
Route::post('saveValidation', [salesController::class,'saveValidation'])->name('saveValidation');
Route::post('/sales/changestatus', 'salesController@changestatus');
Route::post('saveComentarioSales', [salesController::class,'saveComentarioSales'])->name('saveComentarioSales');


Route::post('productssearch', [salesController::class,'productssearch'])->name('productssearch');
Route::post('salesCompleted', [salesController::class,'salesCompleted'])->name('salesCompleted');
Route::post('getAllPerson', [salesController::class,'getAllPerson'])->name('getAllPerson');
Route::post('updateclientsales', [salesController::class,'updateclientsales'])->name('updateclientsales');