<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Models\role;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('descripcion');
            $table->string('codigo');
            $table->boolean('activo');
            $table->timestamps();
        });

        role::create([
            'nombre' => 'Encargado',
            'descripcion' => '',
            'codigo' => 'NCRGD',
            'activo' => true
        ]);

        /*role::create([
            'nombre' => 'Cliente',
            'descripcion' => '',
            'codigo' => 'CLNT',
            'activo' => true
        ]);*/

        role::create([
            'nombre' => 'Contador',
            'descripcion' => '',
            'codigo' => 'CNTDR',
            'activo' => true
        ]);

        role::create([
            'nombre' => 'Supervisor',
            'descripcion' => '',
            'codigo' => 'SPRVSR',
            'activo' => true
        ]);

        role::create([
            'nombre' => 'Anonimo',
            'descripcion' => '',
            'codigo' => 'NNM',
            'activo' => true
        ]);

        role::create([
            'nombre' => 'comprador',
            'descripcion' => '',
            'codigo' => 'CMPRDR',
            'activo' => true
        ]);

        role::create([
            'nombre' => 'Vendedor',
            'descripcion' => '',
            'codigo' => 'VNDDR',
            'activo' => true
        ]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
