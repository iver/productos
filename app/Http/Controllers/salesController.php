<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\file;
use App\Models\products;
use App\Models\sale;
use App\Models\sale_detail;
use App\Models\User;
use App\Models\role;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;


class salesController extends Controller
{
    public function lista(){
        if(Auth::check()){
            return view('sales.sales_home');
        }else{
            return view('layouts.login.login');
        }
    }

    public function liststatus(){
        if(Auth::check()){
            return view('sales.saleslist');
        }else{
            return view('layouts.login.login');
        }
    }

    public function productssearch(Request $request){
        $query = "SELECT p.*, f.archivo FROM products p ".
                  " LEFT JOIN files f ON p.id = f.id_registro  ".
                  " WHERE activo = 1 AND p.status= 'ACEPTADO' AND p.stock > 0 AND (p.nombre LIKE '%".$request->search."%' OR 'NA' = '".$request->search."') GROUP BY p.id";
       
        $results = DB::select($query);
		
        return response(json_encode($results, 200))->header('Content-type','text/plain');
    }

    public function salesCompleted(Request $request){
        //->format('Y-m-d');
        $sales_save = sale::create([
            'nombre' => 'VENTA '.Carbon::now('EDT'),
            'id_usuario' => 0,
            'status' => 'PENDIENTE',
            'fecha_compra' => Carbon::now('EDT'),
            'hora_compra' =>  Carbon::now('EDT')->format('H:i:m'),
            'status_voucher' => 'PENDIENTE',
            'activo' => true
          ]);
          $data = json_decode($request->getContent(), true); //then use
          //dd($data);
         

          foreach($data as $dat)
          { 
              $product = products::find($dat["id"]);
              if($product){
                $sales_detail = sale_detail::create([
                    'nombre' => $product->nombre,
                    'id_producto' => $product->id,
                    'costo' => $product->costo,
                    'cantidad' => $dat["cantidad"],
                    'id_sales' => $sales_save->id,
                    'costo' => $product->costo,
                    'activo' => true
                  ]);

                  $stock = intval($product->stock) - $dat["cantidad"];
                  $stock = $stock > 0 ? $stock : 0;
                  products::find($product->id)->update([
                    'stock' => $stock
                  ]);
            }
          }
        
         
        return response(json_encode($sales_save, 200))->header('Content-type','text/plain');
    }

    public function getAllPerson(Request $request){
        $userId = Auth::id();
        $userdata = User::find($userId);
        $rol = role::find($userdata->id_rol);

        $query = '';
        if($rol->codigo == 'CMPRDR'){
            $query = "SELECT s.* FROM users s WHERE id = ".$userId;            
        }else{
            $query = "SELECT s.* FROM users s ";
        }
        $results = DB::select($query);
        
		
        return response(json_encode($results, 200))->header('Content-type','text/plain');
    }

    public function updateclientsales(Request $request){
       
        $sale = sale::find($request->id)->update([
            'id_usuario' => $request->id_usuario
          ]);

          return response(json_encode($sale, 200))->header('Content-type','text/plain');
    }

    public function allsales(Request $request){
        $userId = Auth::id();
        $userdata = User::find($userId);
        $rol = role::find($userdata->id_rol);

        $voucher = false;
        
        if($rol->codigo == 'CMPRDR') $voucher = true;
        if($rol->codigo == 'VNDDR') $voucher = true;


        $verificarvoucher = false;
        
        if($rol->codigo == 'CNTDR') $verificarvoucher = true;


        $validarentrega = false;
        if($rol->codigo != 'CMPRDR'){
            $validarentrega = true;
        }

        if($request->id_usuario){
            $query = "SELECT s.*, u.nombre, u.paterno, u.materno, SUM(sd.cantidad * sd.costo) as total FROM sales s INNER JOIN sale_details sd ON s.id = sd.id_sales INNER JOIN users u ON s.id_usuario = u.id  WHERE s.id_usuario = ".$request->id_usuario. " GROUP BY s.id";            
            $results = DB::select($query); 
            return response()->json([
                'results' => $results,
                'voucher' => $voucher,
                'verificarvoucher' => $verificarvoucher,
                'validarentrega' => $validarentrega
                ], 200)->header('Content-type','text/plain');
        }else{
            $query = '';
            if($rol->codigo == 'CMPRDR'){
                $query = "SELECT s.*, u.nombre, u.paterno, u.materno, SUM(sd.cantidad * sd.costo) as total FROM sales s INNER JOIN sale_details sd ON s.id = sd.id_sales INNER JOIN users u ON s.id_usuario = u.id  WHERE s.id_usuario = ".$userId. " GROUP BY s.id";            
            }else{
                $query = "SELECT s.*, u.nombre, u.paterno, u.materno, SUM(sd.cantidad * sd.costo) as total  FROM sales s INNER JOIN sale_details sd ON s.id = sd.id_sales INNER JOIN users u ON s.id_usuario = u.id GROUP BY s.id";
            }

            $results = DB::select($query);        
            return response()->json([
                    'results' => $results,
                    'voucher' => $voucher,
                    'verificarvoucher' => $verificarvoucher,
                    'validarentrega' => $validarentrega
                    ], 200)->header('Content-type','text/plain');

        }
        
    }

    public function saveVoucher(Request $request)
    {
        $input['image'] = time().'.'.$request->image->extension();
        $request->image->move(public_path('images'), $input['image']);
        $data = ['nombre' => 'VOUCHER','archivo'=>$input['image'], 'id_registro'=>$request->id];
        file::create($data);
        return response()->json([
          'success'   => 'Image Upload Successfully',
          'uploaded_image' => '<img src="/images/'.$input['image'].'" class="img-thumbnail" width="300" />',
          'class_name'  => 'alert-success'
        ],200);

        
    }


    public function salesid(Request $request){
        $query = "SELECT s.*,s.nombre as nombreventa, u.nombre, u.paterno, u.materno, SUM(sd.cantidad * sd.costo) as total  FROM sales s INNER JOIN sale_details sd ON s.id = sd.id_sales INNER JOIN users u ON s.id_usuario = u.id WHERE s.id = ".$request->id." GROUP BY s.id";
        $results = DB::select($query); 

        
        $sale = sale::find($request->id);
        $query = "SELECT sd.nombre,sd.cantidad, sd.costo, SUM(sd.cantidad * sd.costo) as total  FROM sale_details sd WHERE sd.id_sales = ".$sale->id." GROUP BY sd.id";
        $productos = DB::select($query); 

        $query = "SELECT f.* FROM files f WHERE nombre = 'VOUCHER' AND f.id_registro = ".$request->id;
        $files = DB::select($query); 

        return response()->json([
                'sales' => $results,
                'productos' => $productos,
                'files' => $files
                ], 200)->header('Content-type','text/plain');
    }

    public function changestatus(Request $request){
        $sale_find = sale::find($request->id);
        $status = $sale_find->status == 'ENTREGADO' ? 'PENDIENTE' : 'ENTREGADO';

        $sale = sale::find($request->id)->update([
            'status' => $status
          ]);

        return response(json_encode($sale, 200))->header('Content-type','text/plain');
    }

    public function saveComentarioSales(Request $request){

        $sale = sale::find($request->id)->update([
            'comentario' => $request->comentario
          ]);

        return response(json_encode($sale, 200))->header('Content-type','text/plain');
    }

    public function salesunique(Request $request){
        $sale = sale::find($request->id);

          return response()->json([
            'sales' => $sale
            ], 200)->header('Content-type','text/plain');
    }

    public function saveValidation(Request $request){
        $sale = sale::find($request->id)->update([
            'motivo_validacion' => $request->comentariovalidacion,
            'status_voucher' => $request->status
        ]);

        return response(json_encode($sale, 200))->header('Content-type','text/plain');
    }

}
