<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Models\products;
use DB;
use App\Models\file;

use Session;

class HomeController extends Controller
{
    public function index(){
        if (Auth::check()) {
            return view('home');
        }else{
            return redirect("login");
        }
        
    }

    public function products(){
        $productos = DB::select('SELECT p.id, p.nombre, p.descripcion,p.costo,f.archivo FROM products p LEFT JOIN files f ON p.id = f.id_registro WHERE p.activo  AND p.status = "ACEPTADO" GROUP BY p.id');
        
        return view("sales.ladingpage", ["productos"=>$productos]);

    }

}
