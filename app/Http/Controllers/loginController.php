<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Session;
use Hash;



class loginController extends Controller
{
    public function login(){
        return view('layouts.login.login');
    }

    public function singin(Request $request){
         $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
       
        if (Auth::attempt($credentials)) {
            return redirect('/home');
            //return redirect()->intended('dashboard')
                       // ->with('error', 'Usuario o contraseña incorrecto');
        }
        else{
            return redirect("login")->with('error', 'Usuario o contraseña incorrecto');
        }       
    }

    public function dashboard()
    {
        if(Auth::check()){
            return view('dashboard');
        }
  
        return redirect("login")->withSuccess('You are not allowed to access');
    }
    

    public function signOut() {
        Session::flush();
        Auth::logout();
  
        return Redirect('login');
    }
	

}
