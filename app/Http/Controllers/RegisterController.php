<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Log;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\role;
use Session;
use Hash;
use DB;

class RegisterController extends Controller
{
    public function registro(){
        return view('layouts.login.registro');
    }

    public function postRegister(Request $request){
    	$rules = [
    		'nombre' => 'required',
    		'paterno' => 'required',
    		'email' => 'required|email|unique:users,email',
            
    		'password' => 'required|min:6',
    		'confirmationp' =>'required|min:6|same:password'
    	];
    	$messages =[
    		'nombre.required' => 'su nombre es requerido.',
    		'paterno.required' => 'Su primer apellido es requerido',
    		'email.required' => 'Su correo electronico es requerido.',
    		'email.email' => 'El formato de su correo electronico es invalido',
    		'password.required' => 'Debe ingresar una contraseña',
    		'password.min' => 'La contraseña debe de contener al menos 6 caracteres',
    		'confirmationp.required' => 'Es necesario confirmar la contraseña',
    		'confirmationp.min' => 'La confirmacion de la contraseña debe de contener al menos 6 caracteres',
    		'confirmationp.same' => 'Las contraseña no conciden.'
    	];

		//\Log::debug('Test var fails ' );

    	$validator = Validator::make($request->all(), $rules, $messages);
    	
        if ($validator->fails()):
    		return back()->withErrors($validator);
    	endif;	

		
        $data = $request->all();
        $this->create($data, 1);
        
        return redirect("login")->with('message', '¡Cuenta registrada correctamente!');
    	

    }

	public function create(array $data)
    {
	  $results = DB::select('select * from roles where codigo = ?', ['CMPRDR']);
		
      return User::create([
        'nombre' => $data['nombre'],
        'paterno' => $data['paterno'],
        'materno' => $data['materno'],
        'picture' => '',
        'email' => $data['email'],
        'activo' => 1,
		'id_rol' => $results[0]->id,
		'code_role' => $results[0]->codigo,
        'password' => Hash::make($data['password'])
      ]);
    }   
	
	

	//api
	public function all(Request $request){
        $results = DB::select('select * from users where activo = ?', [true]);
		
        return response(json_encode($results, 200))->header('Content-type','text/plain');
    }

    public function lista(){
        return view('usuarios');
    }

    public function saveUser(Request $request)
    {
     
      
      $validator = Validator::make($request->all(), [
          'nombre' => 'required',
          'paterno' => 'required',
      ]);

     if ($validator->passes()) {
      $email_data = User::where('email', $request->email)->first();
      if($email_data && $email_data->id != $request->id){
        return response()->json([
          'success'   => ['El email ya existe']
        ],502);
      }
      //return response()->json([$request->id_rol],200);
		  $rol_seleccionado = role::find($request->id_rol);

        $flight = User::find($request->id);
        if($flight){
            User::find($flight->id)->update([
                'nombre' => $request->nombre,
                'paterno' => $request->paterno ? $request->paterno : '',
                'materno' => $request->materno ? $request->materno : '',
                'picture' => '',
                'email' => $request->email,
                'id_rol' => $rol_seleccionado->id,
                'code_role' => $rol_seleccionado->codigo,
                'password' => $request->password?Hash::make($request->password) : $flight->password,
                'activo' => true
              ]);
          
          return response()->json([],200);
        }else{
          $usr = User::create([
            'nombre' => $request->nombre,
            'paterno' => $request->paterno ? $request->paterno : '',
            'materno' => $request->materno ? $request->materno : '',
			      'picture' => '',
            'email' => $request->email,
            'id_rol' => $rol_seleccionado->id,
            'code_role' => $rol_seleccionado->codigo,
            'password' => Hash::make($request->password),
            'activo' => true
          ]);
            
		  return response()->json([
              'success'   => 'Image Upload Successfully',
              'class_name'  => 'alert-success'
            ],200);
        }
    
    }else{
      return response()->json(['error'=>$validator->errors()->all()], 202);      
    }
        
    }

    public function editUser(Request $request){  
        $results = DB::select('select * from users where id = ?', [$request->id]);

        $roles = DB::select('select * from roles where activo = ?', [true]);
		
        return response()->json([
                    'user' => ($results && count($results) > 0 ? $results[0] : null),
                    'roles' => $roles
                    ], 200)->header('Content-type','text/plain');
    }

    public function allroles(Request $request){  
      $roles = DB::select('select * from roles where activo = ?', [true]);
  
      return response()->json([
                  'roles' => $roles
                  ], 200)->header('Content-type','text/plain');
    }


    public function deleteUser(Request $request){
        User::find($request->id)->update([
          'activo' => false
        ]);
        
        return response()->json([],200);
    }

}
