<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\file;
use App\Models\products;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\role;

class productController extends Controller
{
    public function all(Request $request){
        $userId = Auth::id();
        $userdata = User::find($userId);
        $rol = role::find($userdata->id_rol);

        $validar_producto = false;
        if($rol->codigo == 'SPRVSR'){
          $validar_producto = true;
        }
        if($rol->codigo == 'NCRGD'){
          $validar_producto = true;
        }

        $results = DB::select('select * from products where activo = ?', [true]);		

        return response()->json([
          'results' => $results,
          'validar_producto' => $validar_producto
          ], 200)->header('Content-type','text/plain');
          
    }

    public function lista(){
      if(Auth::check()){
        return view('productos');
      }else{
        return view('layouts.login.login');
      }        
    }

    public function saveProduct(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'nombre' => 'required',
        'costo' => 'required|min:0',
        'stock' => 'required|min:0',
        //'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);
     if ($validator->passes()) {
        $flight = products::find($request->id);
        if($flight){
          products::find($flight->id)->update([
            'nombre' => $request->nombre,
            'descripcion' => $request->descripcion ? $request->descripcion : '',
            'costo' => $request->costo,
            'stock' => $request->stock,
            'status' => $request->status,
            'comentario' => $request->comentario ? $request->comentario : '',
            'id_categoria' => $request->id_categoria,
            'activo' => true
          ]);

          
          return response()->json([],200);
        }else{
          $prodct = products::create([
            'nombre' => $request->nombre,
            'descripcion' => $request->descripcion ? $request->descripcion : '',
            'costo' => $request->costo,
            'stock' => $request->stock,
            'status' => $request->status,
            'comentario' => $request->comentario ? $request->comentario : '',
            'id_categoria' => $request->id_categoria,
            'activo' => true
          ]);

            $input['image'] = time().'.'.$request->image->extension();
            $request->image->move(public_path('images'), $input['image']);
            $data = ['nombre' => 'PRODUCTO','archivo'=>$input['image'], 'id_registro'=>$prodct->id];
            file::create($data);
            return response()->json([
              'success'   => 'Image Upload Successfully',
              'uploaded_image' => '<img src="/images/'.$input['image'].'" class="img-thumbnail" width="300" />',
              'class_name'  => 'alert-success'
            ],200);

        }
    
    }else{
      return response()->json(['error'=>$validator->errors()->all()], 202);      
    }
        
    }

    public function editProduct(Request $request){  
        $results = DB::select('select * from products where id = ?', [$request->id]);

        $results_file = DB::select('select * from files where id_registro = ? AND nombre = "PRODUCTO"', [$results[0]->id]);
		
        return response()->json([
                    'producto' => ($results && count($results) > 0 ? $results[0] : null),
                    'file'=> ($results_file && count($results_file) > 0 ? $results_file[0] : null)
                    ], 200)->header('Content-type','text/plain');
    }

    public function deleteProduct(Request $request){
        products::find($request->id)->update([
          'activo' => false
        ]);
        
        return response()->json([],200);
    }

}
