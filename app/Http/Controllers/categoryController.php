<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\file;
use App\Models\category;
use DB;
use Illuminate\Support\Facades\Auth;


class categoryController extends Controller
{
    public function all(Request $request){
        $results = DB::select('select * from categories where activo = ?', [true]);
		
        return response(json_encode($results, 200))->header('Content-type','text/plain');
    }

    public function lista(){
      if(Auth::check()){
        return view('category');
      }else{
          return view('layouts.login.login');
      }        
    }

    public function savecategory(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'nombre' => 'required',
      ]);
     if ($validator->passes()) {
        $flight = category::find($request->id);
        if($flight){
            category::find($flight->id)->update([
            'nombre' => $request->nombre,
            'descripcion' => $request->descripcion ? $request->descripcion : '',
            'activo' => true
          ]);

          return response()->json([],200);
        }else{
          $categ = category::create([
            'nombre' => $request->nombre,
            'descripcion' => $request->descripcion ? $request->descripcion : '',
            'activo' => true
          ]);

            $input['image'] = time().'.'.$request->image->extension();
            $request->image->move(public_path('images'), $input['image']);
            $data = ['nombre' => 'CATEGORIA','archivo'=>$input['image'], 'id_registro'=>$categ->id];
            file::create($data);
            return response()->json([
              'success'   => 'Image Upload Successfully',
              'uploaded_image' => '<img src="/images/'.$input['image'].'" class="img-thumbnail" width="300" />',
              'class_name'  => 'alert-success'
            ],200);

        }
    
    }else{
      return response()->json(['error'=>$validator->errors()->all()], 502);      
    }
        
    }
    
    public function editcategory(Request $request){  
        $results = DB::select('select * from categories where id = ?', [$request->id]);
       
        $results_file = DB::select('select * from files where id_registro = ? AND nombre = "CATEGORIA"', [$results[0]->id]);
		
        return response()->json([
                    'category' => ($results && count($results) > 0 ? $results[0] : null),
                    'file'=> ($results_file && count($results_file) > 0 ? $results_file[0] : null)
                    ], 200)->header('Content-type','text/plain');
    }

    public function deletecategory(Request $request){
        category::find($request->id)->update([
          'activo' => false
        ]);
        
        return response()->json([],200);
    }

    public function allcat(Request $request){  
        $roles = DB::select('select * from categories where activo = ?', [true]);
    
        return response()->json([
                    'category' => $roles
                    ], 200)->header('Content-type','text/plain');
      }

}
