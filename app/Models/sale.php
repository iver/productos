<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sale extends Model
{
    use HasFactory;
    protected $fillable = [
        'nombre',
        'id_usuario',
        'status',
        'fecha_compra',
        'hora_compra',
        'activo',
        'status_voucher',
        'activo',
        'comentario',
        'motivo_validacion'
    ];
}

